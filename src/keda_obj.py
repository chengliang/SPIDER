# -*- coding:utf-8 -*-

import urllib2
import json
import csv

class TEACHER:
    def __init__(self):
        self.teacherid=0
        self.name=""
        self.sectionId=0
        self.sectionName=""
        self.titleId=0
        self.titleName=""
        self.position=""
        self.picurl=""
        self.email=""
        self.telephone=""
        self.resume=""
        self.research=""
        self.project=""
        self.paper=""
        self.work=""
        self.remark=""
        

class KEDA:
    #初始化
    def __init__(self):
        self.url = 'http://www.ccse.uestc.edu.cn:8080/user/index'
        self.user_agent='Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
        #初始化headers
        self.headers = {'User-Agent' : self.user_agent}
        #存放每专业的变量，每一个元素是每个级别的list
        self.teachers = []
        self.enable = False

    #获取该secId和titId指向的Json
    def getJson(self, url):
        try:
            #构建request
            request = urllib2.Request(url, headers = self.headers)
            #利用urlopen获取页面代码
            response = urllib2.urlopen(request)
            #从response中解析json
            result = json.load(response)
            return result
    
        except urllib2.URLError, e:
            if hasattr(e,"reason"):
                print u"连接官网失败，错误原因：", e.reason
                return None

    #通过json获取教师信息页面的json
    def getInfoJson(self, ori_json):
        total = ori_json['total']            
        #循环读出教师id获取教师信息页面的json
        for i in range(0, total-1):
            try:
                id = ori_json['list'][i]['id']
#                print "id++++++++", id
                teacherInfo = self.getJson('http://www.ccse.uestc.edu.cn:8080/user/view/' + str(id))
                teacher = TEACHER()
                teacher.teacherid = id
                teacher.name = teacherInfo['object']['name'].encode('utf-8')
                teacher.sectionId=teacherInfo['object']['sectionId']
                teacher.sectionName = teacherInfo['object']['sectionName'].encode('utf-8')
                teacher.titleId = teacherInfo['object']['titleId']
                teacher.titleName=teacherInfo['object']['titleName'].encode('utf-8')
                teacher.position = teacherInfo['object']['position'].encode('utf-8')
                teacher.picurl = teacherInfo['object']['picurl'].encode('utf-8')
                teacher.email = teacherInfo['object']['email']
                teacher.telephone = teacherInfo['object']['telephone'].encode('utf-8')
                teacher.resume = teacherInfo['object']['resume'].encode('utf-8')
                teacher.research = teacherInfo['object']['research'].encode('utf-8')
                teacher.project = teacherInfo['object']['project'].encode('utf-8')
                teacher.paper = teacherInfo['object']['paper'].encode('utf-8')
                teacher.work = teacherInfo['object']['work'].encode('utf-8')
                teacher.remark = teacherInfo['object']['remark'].encode('utf-8')
                print teacher.name
                self.teachers.append(teacher)
            except urllib2.URLError, e:
                if hasattr(e, "reason"):
                    print u"连接教师信息失败，错误原因：" , e.reason
                    return None

    #获取所有教师信息并添加到teacher的list中
    def start(self):
        for i in range(1,5):
            for j in range(1,3):
                result = self.getJson('http://www.ccse.uestc.edu.cn:8080/user/index'+'?titleId='+str(j)+'&sectionId='+str(i))
                self.getInfoJson(result)

spider = KEDA()
spider.start()
with open('/home/liang/keda.csv', 'wb') as csvfile:
    spamwriter = csv.writer(csvfile,dialect='excel')
    spamwriter.writerow(['姓名', '系别', '专业技术职务', 'email', '研究方向'])
    for teacher in spider.teachers:
        spamwriter.writerow([teacher.name, teacher.sectionName, teacher.position, teacher.email, teacher.research])
