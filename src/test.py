# -*- coding:utf-8 -*-

import urllib2
import re

class CITY:
    def __init__(self):
        self.cityId=0
        self.date=1
        self.name=""
        self.aqi=""

class DATACENTER:
    #init
    def __init__(self):
        self.pageIndex = 1
        self.user_agent='Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
        #初始化headers
        self.headers = {'User-Agent' : self.user_agent}
        #存放每个城市对象的变量，每个元素是每个城市的对象
        self.citydata = []
        self.enable = False

    #传入每一页的索引获得页面代码
    def getPage(self, pageIndex, startdate, enddate):
        try:
            url = "http://datacenter.mep.gov.cn/report/air_daily/air_dairy.jsp?city=&startdate=2016-01-" + str(startdate) + "&enddate=2016-01-"+ str(enddate) +"&page=" + str(pageIndex)
            request = urllib2.Request(url,headers= self.headers)
            response = urllib2.urlopen(request)
            pageCode = response.read().decode('utf-8', 'ignore')
            return pageCode
        except urllib2.URLError, e:
            if hasattr(e, "reason"):
                print u"链接数据中心失败，错误原因： " + e.reason
                return None

    def getEachItem(self, pageIndex,startdate, enddate):
        pageCode = self.getPage(pageIndex, startdate, enddate)

        #print pageCode

        if not pageCode:
            print "页面加载失败"
            return None

        pattern = re.complile('<area.*?" title="(.*?)">')
        items = re.findall(pattern, pageCode)

        for item in items:
            print item[0]


    def start(self):
        print u"正在读取数据中心"
        self.enable = True

        self.getEachItem(1, 20, 21)

spider = DATACENTER()
spider.start()
